from data_tools.MongoToTFR.tfr_util import parse_tfr_generator


def tfrecord_counter(tfr_path):
    record_iterator = parse_tfr_generator(tfr_path)
    count = 0
    for record in record_iterator:
        """
        put here your constrains of counting things or put a breakpoint to inspect the tfr
        """
        count += 1
    print(count)


if __name__ == '__main__':
    path = '/hdd/data/noga/train/tfrecords/original_tfrecords/to_cloud/spectro_vids_train_vis.record'
    tfrecord_counter(tfr_path=path)