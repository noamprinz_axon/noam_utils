def validate_arguments(args, arguments_list):
    """
    Gets an args input from argparser and arguments list to check if exist or missing, returns and error message with
    the list of the missing values
    """
    missing_arguments = []
    for argument in arguments_list:
        argument_value = getattr(args, argument)
        if not argument_value:
            missing_arguments.append(f"--{argument}")
    if len(missing_arguments) > 0:
        raise ValueError(
            f"You should add the following flags: {missing_arguments}")
