import os
import pandas as pd


def subdirs_to_csv(dir_path, output_path=None, output_filename='output'):
    if output_path is None:
        output_path = dir_path

    subdirs = os.listdir(dir_path)

    dir_names = []
    for dir in subdirs:
        full_path = os.path.join(dir_path, dir)
        if os.path.isdir(full_path):
            dir_names.append(dir)
    dir_names = sorted(dir_names)
    df = pd.DataFrame({'folder_names': dir_names})
    file_name = f"{output_filename}.csv"
    full_output_path = os.path.join(output_path, file_name)
    df.to_csv(full_output_path)


if __name__ == '__main__':
    subdirs_to_csv(dir_path='/home/noam/Desktop/frames_full_fps_black', output_path='/home/noam/Desktop/channels tagging', output_filename='spectro_v2')
