# under data_tools

def filter_4k_images_sergey(dir, out_dir):
    for file in os.listdir(dir):
        img_path = pjoin(dir, file)
        if not cf.is_pic(img_path):
            continue
        else:
            img = cv2.imread(img_path)
            if img is not None and img.shape == (2160, 3840, 3):
                out_path = pjoin(out_dir, file)
                cv2.imwrite(out_path, img)
                cv2.waitKey(0)
                pass


def move_4k_xmls_sergey(img_dir, xml_dir):
    for img in os.listdir(img_dir):
        xml_name = img[:-3] + 'xml'
        xml_path = pjoin(xml_dir, xml_name)
        xml_out_path = pjoin(img_dir, 'xml', xml_name)
        if os.path.exists(xml_path):
            shutil.copy(xml_path, xml_out_path)