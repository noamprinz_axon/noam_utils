from data_tools.MongoToTFR import summary_csv
import time
import pandas as pd
import pandas_profiling as pp
import os
import cv2
import copy
from data_tools.MongoToTFR.summary_csv import add_axis_cols
import time

CORRECT_LABELS = ['person', 'private', 'commercial', 'pickup', 'truck', 'bus', 'van', 'tractor']

CORRECT_COLORS = ['white', 'black', 'silver', 'gray', 'blue', 'yellow', 'brown', 'red', 'green', 'orange']



def plot_bbox(img, bbox, label, color=(0, 0, 255), thickness=1, resize_ratio=0.1):
  '''
  :param img: PIL image
  :param tags:
  :param color:
  :param thickness:
  :return:
  '''
  # bbox = resize(bbox, resize_ratio)
  xmin = bbox[0]
  xmax = bbox[2]
  ymin = bbox[1]
  ymax = bbox[3]
  # plot tags
  image = copy.deepcopy(img)
  tag_top_left_pixel = (int(xmin), int(ymin))
  tag_bottom_right_pixel = (int(xmax), int(ymax))
  cv2.rectangle(image, tag_top_left_pixel, tag_bottom_right_pixel, color=color, thickness=thickness)
  cv2.putText(image, label, tag_top_left_pixel, 1, color=color, fontScale=1.4, thickness=2)
  return image

def filter_by(df, col_name, value, inverse=False):
  if not inverse:
    df = df[df[col_name] == value].reset_index(drop=True)
  else:
    df = df[df[col_name] != value].reset_index(drop=True)
  return df

def filter_by_size(df, col_name, value = 25, less_then=False):

  if not less_then:
    df = df[df[col_name] >= value].reset_index(drop=True)
  else:
    df = df[df[col_name] < value].reset_index(drop=True)
  return df


def exchange_labels(label_list):
    if len(label_list) == 1:
        label_list = label_list[0]
    else:
        label_list = list(set(label_list))
    # if zone == 'MB2':
    if 'heavily occluded' in label_list:
        label_list = ['remove_tag']

    if 'none_relevant' in label_list:
        label_list = ['none_relevant']

    if 'person' in label_list:
        label_list = ['person']

    if 'private' in label_list:
        label_list = ['private']

    if 'commercial' in label_list:
        label_list = ['commercial']

    if 'pick-up closed' in label_list:
        label_list = ['commercial']

    if 'pick_up_closed' in label_list:
        label_list = ['commercial']

    if 'pickupclose' in label_list:
        label_list = ['commercial']

    if 'pick up closed' in label_list:
        label_list = ['commercial']

    if 'station' in label_list:
        label_list = ['commercial']

    if 'pick-up open' in label_list:
        label_list = ['pickup']

    if 'pickupopen' in label_list:
        label_list = ['pickup']

    if 'pick_up_open' in label_list:
        label_list = ['pickup']

    if 'pick up open' in label_list:
        label_list = ['pickup']

    if 'truck' in label_list:  # many double tags of truck and cabin that are bounding the entire truck
        label_list = ['truck']

    if 'bus' in label_list:
        label_list = ['bus']

    if 'van (including mini - bus)' in label_list:  # many double tags of truck and cabin that are bounding the entire truck
        label_list = ['van']

    if 'van' in label_list:  # many double tags of truck and cabin that are bounding the entire truck
        label_list = ['van']

    if 'tractor with' in label_list:  # many double tags of truck and cabin that are bounding the entire truck
        label_list = ['tractor']

    if 'tractor_with' in label_list:  # many double tags of truck and cabin that are bounding the entire truck
        label_list = ['tractor']

    if 'tractor without' in label_list:  # many double tags of truck and cabin that are bounding the entire truck
        label_list = ['tractor']

    if 'tractor_without' in label_list:  # many double tags of truck and cabin that are bounding the entire truck
        label_list = ['tractor']

    if 'tractor' in label_list:  # many double tags of truck and cabin that are bounding the entire truck
        label_list = ['tractor']

    if 'forklift' in label_list:  # many double tags of truck and cabin that are bounding the entire truck
        label_list = ['tractor']

    if 'motorcycle' in label_list: # not very good. many double tagging of private and motorcycle that are either one of them
        label_list = ['remove_tag']

    if 'cabin' in label_list:
        label_list = ['remove_tag']

    if 'car' in label_list:
        label_list = ['private']

    if 'gray' in label_list:
        label_list = ['private']

    if 'brown' in label_list:
        label_list = ['private']

    if 'silver' in label_list:
        label_list = ['private']

    if 'crowded' in label_list:
        label_list = ['none_relevant']

    if 'not occluded' in label_list and 'not crowded' in label_list:
        label_list = ['bus']

    if 'other color' in label_list:
        label_list = ['none_relevant']

    if 'not occluded' in label_list:
        label_list = ['none_relevant'] # Lot of different tags like this

    if 'partly occluded' in label_list:
        label_list = ['none_relevant'] # Lot of different tags like this

    if 'not crowded' in label_list:
        label_list = ['none_relevant'] # Lot of different tags like this

    if 'lightly occluded' in label_list:
        label_list = ['none_relevant'] # Lot of different tags like this

    if 'close' in label_list:
        label_list = ['none_relevant'] # Lot of different tags like this

    return label_list



def exchange_colors(label_list):
    label_list_new = copy.deepcopy(label_list)
    if len(label_list_new) == 1:
        label_list_new = label_list_new[0]
    else:
        label_list_new = list(set(label_list_new))

    if True in ['heavily occluded' in label for label in label_list_new]:
        label_list_new = ['heavily_occluded']

    if True in ['heavily_occluded' in label for label in label_list_new]:
        label_list_new = ['heavily_occluded']

    if True in ['white' in label for label in label_list_new]:
        label_list_new = ['white']

    if True in ['black' in label for label in label_list_new]:
        label_list_new = ['black']

    if True in ['silver' in label for label in label_list_new]:
        label_list_new = ['silver']

    if True in ['grey' in label for label in label_list_new]:
        label_list_new = ['gray']

    if True in ['gray' in label for label in label_list_new]:
        label_list_new = ['gray']

    if True in ['blue' in label for label in label_list_new]:
        label_list_new = ['blue']

    if True in ['yellow' in label for label in label_list_new]:
        label_list_new = ['yellow']

    if True in ['brown' in label for label in label_list_new]:
        label_list_new = ['brown']

    if True in ['red' in label for label in label_list_new]:
        label_list_new = ['red']

    if True in ['green' in label for label in label_list_new]:
        label_list_new = ['green']

    if True in ['orange' in label for label in label_list_new]:
        label_list_new = ['orange']

    if True in ['other_color' in label for label in label_list_new]:
        label_list_new = ['other_color']

    if True in ['other color' in label for label in label_list_new]:
        label_list_new = ['other_color']

    return label_list_new


def main():

    # all_csvs_path ='/hdd/MB2/statistics/relevant_csvs'
    # all_xmls = '/hdd/MB2/statistics/all_xmls'
    # profile_reports = '/hdd/MB2/statistics/relevant_csvs/profile_reports'
    zone = 'AGG_DATA'
    count = 0
    all_zones = sorted(['/hdd/data/magicbox/images/lines_df.csv'])
    tim = time.time()
    for path in all_zones:
        new_label_col = []
        new_color_col = []
        data = pd.read_csv(path)
        for i in range(len(data['label'])):
            label_list = data['label'][i].split(',')
            if len(label_list) == 1:
                label_list = [label_list]
            new_label = exchange_labels(label_list)[0]
            new_label_col.append(new_label)
        data['new_label'] = new_label_col
        endtime = time.time()
        print(endtime-tim)
        colors_path = path[:-4] + '_with_colors.csv'
        data.to_csv(colors_path, index=False)

    csv_path = '/hdd/data/magicbox/images/lines_df_with_colors.csv'
    raw_data = pd.read_csv(csv_path)
    colros_dataset_dir = '/hdd/data/magicbox/images/color'

    for color in CORRECT_COLORS:
      color_dir_path = os.path.join(colros_dataset_dir, color)
      if not os.path.exists(color_dir_path):
        os.makedirs(color_dir_path)

    # raw_data = pd.read_csv('/hdd/MB2/statistics/relevant_csvs/MB2.csv')
    # raw_data = data


    pass
    #raw_data = add_axis_cols(raw_data)

    dir_col = []
    zone_col = []
    for path in raw_data['image_path']:
      dir = path.split('/')[-2]
      zone = path.split('/')[-3]
      dir_col.append(dir)
      zone_col.append(zone)
    raw_data['zone'] = zone_col
    raw_data['dir'] = dir_col
    paths_col = []
    labels_col = []
    colors_col = []

    resize_ratio = 2
    all_file_names = list(set(list(raw_data['image_path'])))
    for image_path in all_file_names:
      # image_path = xml_path.replace('/statistics/all_xmls/', '/data_MB2/').replace('_xmls', '').replace('.xml', '.jpg')
      # image_path = xml_path.replace('_xmls','').replace('.xml', '.jpg')


      # relevant_tags_df = filter_by(raw_data, 'path', xml_path)
      relevant_tags_df = filter_by(raw_data, 'image_path', image_path)
      for i in range(len(relevant_tags_df['image_path'])):
        xmin = max(relevant_tags_df['xmin'][i],0)
        xmax = relevant_tags_df['xmax'][i]
        ymin = max(relevant_tags_df['ymin'][i],0)
        ymax = relevant_tags_df['ymax'][i]
        zone = relevant_tags_df['zone'][i]
        dir = relevant_tags_df['dir'][i]
        label = relevant_tags_df['new_label'][i]
        color = relevant_tags_df['color'][i]
        if color in CORRECT_COLORS:
          count+=1
          img = cv2.imread(image_path)
          if img is None:
            continue
          # bbox = [xmin, ymin, xmax, ymax]
          img_crop = img[int(ymin):int(ymax), int(xmin):int(xmax)]


          new_path = os.path.join(colros_dataset_dir, color, str(count) + '_' + str(label) + '_' + str(zone)+'_' + str(dir) + '.jpg')
          try:
            cv2.imwrite(new_path, img_crop)
            paths_col.append(new_path)
            labels_col.append(label)
            colors_col.append(color)
            print(count)
          except:
            print('{} was not successful'.format(new_path))
            pass

    colors_data = pd.DataFrame(zip(paths_col, labels_col, colors_col), columns=['path', 'label', 'color'])
    colors_data.to_csv(os.path.join(colros_dataset_dir, 'colors_data.csv'), index=False)



      # print('original img size is : {}X {} '.format(img.shape[0], img.shape[1]))
      # img = cv2.resize(img, (int(img.shape[1] / resize_ratio), int(img.shape[0] / resize_ratio)))
      # cv2.imshow(image_path, img)
      # cv2.waitKey(0)
      # cv2.destroyAllWindows()




if __name__ == '__main__':
    main()