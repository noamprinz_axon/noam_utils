from data_tools.DatasetParsers.xml_parser import XmlParser
import os.path
import pandas as pd

def parse_xmls(path):
    parser = XmlParser('./tmp')
    tags = parser.parse_xml_tags(path)
    tags_list = []
    for key, tag in tags.items():
        tags_list.append([tag['xmin'], tag['ymin'], tag['xmax'], tag['ymax'], tag['label'], tag['color'],
                          tag['angle'], tag['occlusion'], tag['appearance']])
    return tags_list

def add_single_image_to_lines(lines, image_path, xml_path):
    tags_lines = parse_xmls(xml_path)
    path_fields = [image_path, xml_path]
    img_name = os.path.basename(image_path)
    img_name = os.path.splitext(img_name)[0]
    img_dirname = os.path.basename(os.path.dirname(image_path))
    frame_num = f'{img_dirname}_{img_name}'
    for tag_line in tags_lines:
        line = [frame_num] + tag_line + path_fields
        lines.append(line)
    return lines

def add_directory_to_lines(lines, dirpath):
    xml_dir_path = os.path.join(dirpath, 'xml')
    for xml_file in os.listdir(xml_dir_path):
        xml_path = os.path.join(xml_dir_path, xml_file)
        image_file = os.path.splitext(xml_file)[0] + '.jpg'
        image_path = os.path.join(dirpath, image_file)
        lines = add_single_image_to_lines(lines, image_path, xml_path)
    return lines




if __name__=='__main__':
    dir_path = '/hdd/data/magicbox/images/images_batch_3'
    lines = []
    # lines = add_single_image_to_lines(lines, image_path, xml_path)
    for dir in os.listdir(dir_path):
        subdir_path = os.path.join(dir_path, dir)
        lines = add_directory_to_lines(lines, subdir_path)
    columns = ['real_frame_num', 'xmin', 'ymin', 'xmax', 'ymax', 'label', 'color', 'angle', 'occlusion', 'appearance', 'image_path', 'xml_path']
    lines_df = pd.DataFrame(lines, columns=columns)
    output_path = os.path.join(dir_path, 'lines_df.csv')
    lines_df.to_csv(output_path)
